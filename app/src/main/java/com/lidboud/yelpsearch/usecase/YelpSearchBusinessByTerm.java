package com.lidboud.yelpsearch.usecase;

import com.lidboud.yelpsearch.data.model.domain.Business;
import com.lidboud.yelpsearch.repository.BusinessRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class YelpSearchBusinessByTerm implements SearchBusinessByTerm {

    private BusinessRepository businessRepository;

    @Inject
    public YelpSearchBusinessByTerm(BusinessRepository businessRepository) {
        this.businessRepository = businessRepository;
    }

    @Override
    public Observable<List<Business>> searchBusinessByTerm(String term) {
        return businessRepository.fetchBusinessByTerm(term);
    }
}
