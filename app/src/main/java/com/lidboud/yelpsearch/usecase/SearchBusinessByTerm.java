package com.lidboud.yelpsearch.usecase;

import com.lidboud.yelpsearch.data.model.domain.Business;

import java.util.List;

import io.reactivex.Observable;

public interface SearchBusinessByTerm {
    Observable<List<Business>> searchBusinessByTerm(String term);
}
