package com.lidboud.yelpsearch.injection.module;

import android.content.Context;

import com.lidboud.yelpsearch.R;
import com.lidboud.yelpsearch.common.YelpAuthorizationInterceptor;
import com.lidboud.yelpsearch.data.datasource.api.BusinessApi;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {

    @Provides
    @Named("SearchLocation")
    String provideLocation() {
        return "montreal";
    }

    @Provides
    @Named("YelpAuthId")
    String provideAuthid(Context context) {
        return context.getString(R.string.yelp_auth_id);
    }

    @Provides
    @Named("AuthorizationInterceptor")
    Interceptor provideAuthorizationInterceptor(@Named("YelpAuthId") String authId) {
        return new YelpAuthorizationInterceptor(authId);
    }

    @Provides
    OkHttpClient provideHttpClient(@Named("AuthorizationInterceptor") Interceptor authInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(authInterceptor)
                .build();
    }

    @Provides
    Retrofit providerRetrofitBuilder(Context context, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(context.getString(R.string.yelp_base_url))
                .build();
    }

    @Provides
    BusinessApi providesBusinessApi(Retrofit retrofit) {
        return retrofit.create(BusinessApi.class);
    }
}