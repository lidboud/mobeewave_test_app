package com.lidboud.yelpsearch.injection.module;

import com.lidboud.yelpsearch.injection.PerActivity;
import com.lidboud.yelpsearch.ui.search.SearchActivity;
import com.lidboud.yelpsearch.ui.search.SearchActivityModule;
import com.lidboud.yelpsearch.ui.search.SearchViewModelModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityProviderModule {

    @PerActivity
    @ContributesAndroidInjector(modules = {SearchActivityModule.class, SearchViewModelModule.class})
    abstract SearchActivity contributeSearchActivity();
}
