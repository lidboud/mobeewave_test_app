package com.lidboud.yelpsearch.injection.module;

import android.app.Application;
import android.content.Context;

import com.lidboud.yelpsearch.YelpApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    @Provides
    @Singleton
    Application provideApplication(YelpApplication application) {
        return application;
    }

    @Provides
    @Singleton
    Context provideContext(Application application) {
        return application.getApplicationContext();
    }
}