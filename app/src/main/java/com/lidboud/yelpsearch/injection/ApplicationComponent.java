package com.lidboud.yelpsearch.injection;

import com.lidboud.yelpsearch.YelpApplication;
import com.lidboud.yelpsearch.injection.module.ActivityProviderModule;
import com.lidboud.yelpsearch.injection.module.ApiModule;
import com.lidboud.yelpsearch.injection.module.ApplicationModule;
import com.lidboud.yelpsearch.injection.module.ViewModelFactoryModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        ApiModule.class,
        ApplicationModule.class,
        ActivityProviderModule.class,
        ViewModelFactoryModule.class})
public interface ApplicationComponent extends AndroidInjector<YelpApplication> {

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<YelpApplication> {
    }
}
