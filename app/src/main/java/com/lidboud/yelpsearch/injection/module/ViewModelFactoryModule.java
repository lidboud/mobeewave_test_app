package com.lidboud.yelpsearch.injection.module;

import android.arch.lifecycle.ViewModelProvider;

import com.lidboud.yelpsearch.injection.ViewModelFactory;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class ViewModelFactoryModule {

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);
}
