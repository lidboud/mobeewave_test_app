package com.lidboud.yelpsearch.repository;

import com.lidboud.yelpsearch.data.datasource.BusinessDataSource;
import com.lidboud.yelpsearch.data.model.domain.Business;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class YelpBusinessRepository implements BusinessRepository {

    private BusinessDataSource businessDataSource;

    @Inject
    public YelpBusinessRepository(BusinessDataSource businessDataSource) {
        this.businessDataSource = businessDataSource;
    }

    @Override
    public Observable<List<Business>> fetchBusinessByTerm(String term) {
        return businessDataSource.fetchBusinessByTerm(term);
    }
}