package com.lidboud.yelpsearch.repository;

import com.lidboud.yelpsearch.data.model.domain.Business;

import java.util.List;

import io.reactivex.Observable;

public interface BusinessRepository {
    Observable<List<Business>> fetchBusinessByTerm(String term);
}
