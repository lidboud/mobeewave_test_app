package com.lidboud.yelpsearch.data.datasource;

import com.lidboud.yelpsearch.data.model.domain.Business;

import java.util.List;

import io.reactivex.Observable;

public interface BusinessDataSource {
    Observable<List<Business>> fetchBusinessByTerm(String term);
}
