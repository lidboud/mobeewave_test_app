package com.lidboud.yelpsearch.data.model.domain;

import com.google.gson.annotations.SerializedName;

public class Business {

    @SerializedName("name")
    private String name;
    @SerializedName("phone")
    private String phone;

    public Business(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }
}
