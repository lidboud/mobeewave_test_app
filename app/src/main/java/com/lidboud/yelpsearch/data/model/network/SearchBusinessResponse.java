package com.lidboud.yelpsearch.data.model.network;

import com.google.gson.annotations.SerializedName;
import com.lidboud.yelpsearch.data.model.domain.Business;

import java.util.ArrayList;
import java.util.List;

public class SearchBusinessResponse {

    @SerializedName("businesses")
    private List<Business> businesses = new ArrayList<>();

    public List<Business> getBusinesses() {
        return businesses;
    }
}
