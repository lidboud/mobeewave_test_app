package com.lidboud.yelpsearch.data.datasource.api;

import com.lidboud.yelpsearch.data.model.network.SearchBusinessResponse;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface BusinessApi {
    @GET("v3/businesses/search")
    Observable<SearchBusinessResponse> searchBusiness(@QueryMap Map<String, String> params);
}
