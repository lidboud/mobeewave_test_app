package com.lidboud.yelpsearch.data.datasource;

import com.lidboud.yelpsearch.data.datasource.api.BusinessApi;
import com.lidboud.yelpsearch.data.model.domain.Business;
import com.lidboud.yelpsearch.data.model.network.SearchBusinessResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class YelpNetworkBusinessDataSource implements BusinessDataSource {

    private BusinessApi businessApi;
    private String location;

    @Inject
    public YelpNetworkBusinessDataSource(BusinessApi businessApi, @Named("SearchLocation") String location) {
        this.businessApi = businessApi;
        this.location = location;
    }

    @Override
    public Observable<List<Business>> fetchBusinessByTerm(final String term) {
        Map<String, String> query = new HashMap<String, String>() {{
            put("location", location);
            put("term", term);
        }};

        return businessApi.searchBusiness(query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(SearchBusinessResponse::getBusinesses);
    }
}