package com.lidboud.yelpsearch.common;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class YelpAuthorizationInterceptor implements Interceptor {

    private static final String BEARER_TITLE = "Bearer ";
    private String authId;

    @Inject
    public YelpAuthorizationInterceptor(String authId) {
        this.authId = authId;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request.Builder builder = chain.request().newBuilder();
        builder.header("Authorization", BEARER_TITLE + authId);
        return chain.proceed(builder.build());
    }
}
