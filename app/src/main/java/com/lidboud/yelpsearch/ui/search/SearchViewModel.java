package com.lidboud.yelpsearch.ui.search;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.lidboud.yelpsearch.R;
import com.lidboud.yelpsearch.data.model.domain.Business;
import com.lidboud.yelpsearch.usecase.SearchBusinessByTerm;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;

public class SearchViewModel extends AndroidViewModel {
    private MutableLiveData<SearchDataModel> businessesData;
    private SearchBusinessByTerm searchBusinessByTerm;
    private CompositeDisposable disposables;

    @Inject
    public SearchViewModel(@NonNull Application application,
                           MutableLiveData<SearchDataModel> businessesData,
                           SearchBusinessByTerm searchBusinessByTerm) {
        super(application);
        this.businessesData = businessesData;
        this.searchBusinessByTerm = searchBusinessByTerm;
        disposables = new CompositeDisposable();
    }

    @Override
    protected void onCleared() {
        disposables.dispose();
    }

    public LiveData<SearchDataModel> getSearchedBusinessesData() {
        return businessesData;
    }

    public void searchBusinessByTerm(String term) {
        if (isTermValid(term)) {
            notifyError(getFormatErrorMessage());
        } else {
            notifyLoadingState();
            disposables.add(searchBusinessByTerm
                    .searchBusinessByTerm(term)
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::handleSuccess, error -> handleError()));
        }
    }

    private void notifyLoadingState() {
        SearchDataModel searchDataModel = new SearchDataModel();
        searchDataModel.setLoading(true);
        businessesData.postValue(searchDataModel);
    }

    private void handleSuccess(List<Business> businesses) {
        SearchDataModel searchDataModel = new SearchDataModel();
        searchDataModel.setBusinesses(businesses);
        searchDataModel.setLoading(false);
        businessesData.postValue(searchDataModel);
    }

    private void handleError() {
        notifyError(getGenericMessage());
    }

    private void notifyError(String errorMessage) {
        SearchDataModel searchDataModel = new SearchDataModel();
        searchDataModel.setLoading(false);
        searchDataModel.setErrorMessage(errorMessage);
        businessesData.postValue(searchDataModel);
    }

    private Boolean isTermValid(String term) {
        return TextUtils.isEmpty(term);
    }

    private String getGenericMessage() {
        return getApplication().getString(R.string.genericError);
    }

    private String getFormatErrorMessage() {
        return getApplication().getString(R.string.formatError);
    }
}
