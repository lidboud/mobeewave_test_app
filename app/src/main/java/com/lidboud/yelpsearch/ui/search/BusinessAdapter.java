package com.lidboud.yelpsearch.ui.search;

import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.lidboud.yelpsearch.data.model.domain.Business;
import com.lidboud.yelpsearch.databinding.BusinessItemBinding;

import javax.inject.Inject;

public class BusinessAdapter extends ListAdapter<Business, BusinessAdapter.ViewHolder> {

    @Inject
    public BusinessAdapter(DiffUtil.ItemCallback<Business> businessDiffCallback) {
        super(businessDiffCallback);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(BusinessItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Business business = getItem(i);
        viewHolder.bind(business);
        viewHolder.itemView.setTag(business);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private BusinessItemBinding businessItemBinding;

        ViewHolder(@NonNull BusinessItemBinding businessItemBinding) {
            super(businessItemBinding.getRoot());
            this.businessItemBinding = businessItemBinding;
        }

        void bind(Business business) {
            businessItemBinding.setBusiness(business);
            businessItemBinding.executePendingBindings();
        }
    }
}

class BusinessDiffCallback extends DiffUtil.ItemCallback<Business> {

    @Override
    public boolean areItemsTheSame(@NonNull Business oldItem, @NonNull Business newItem) {
        return oldItem.getName().equals(newItem.getName()) && oldItem.getPhone().equals(newItem.getPhone());
    }

    @Override
    public boolean areContentsTheSame(@NonNull Business oldItem, @NonNull Business newItem) {
        return oldItem == newItem;
    }
}
