package com.lidboud.yelpsearch.ui.search;

import com.lidboud.yelpsearch.data.model.domain.Business;

import java.util.List;

public class SearchDataModel {
    private List<Business> businesses;
    private String errorMessage;
    private boolean isLoading;

    public SearchDataModel() {
    }

    public SearchDataModel(List<Business> businesses, String errrorMessage) {
        this.businesses = businesses;
        this.errorMessage = errrorMessage;
    }

    public List<Business> getBusinesses() {
        return businesses;
    }

    public void setBusinesses(List<Business> businesses) {
        this.businesses = businesses;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }
}
