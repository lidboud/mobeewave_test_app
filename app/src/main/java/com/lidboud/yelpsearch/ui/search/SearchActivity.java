package com.lidboud.yelpsearch.ui.search;

import android.app.Dialog;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;

import com.lidboud.yelpsearch.R;
import com.lidboud.yelpsearch.data.model.domain.Business;

import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.support.DaggerAppCompatActivity;

public class SearchActivity extends DaggerAppCompatActivity {

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    @Inject
    ListAdapter<Business, BusinessAdapter.ViewHolder> listAdapter;

    SearchViewModel setupViewModel;
    private RecyclerView businessList;

    public void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        businessList = findViewById(R.id.search_layout_recyclerView);
        initViewModel();
        setupRecyclerView();
        setupListener();
    }

    private void initViewModel() {
        setupViewModel = ViewModelProviders.of(this, viewModelFactory).get(SearchViewModel.class);
        setupViewModel();
    }

    private void setupListener() {
        findViewById(R.id.search_layout_buttom).setOnClickListener(v -> {
            clearSearchResult();
            String term = ((EditText) findViewById(R.id.search_layout_term_editText)).getText().toString();
            setupViewModel.searchBusinessByTerm(term);
        });
    }

    private void setupRecyclerView() {
        businessList.setLayoutManager(new LinearLayoutManager(this));
        businessList.setAdapter(listAdapter);
    }

    private void setupViewModel() {
        setupViewModel.getSearchedBusinessesData().observe(this, searchDataModel -> {
            if (searchDataModel != null) {
                handleLoadingState(searchDataModel.isLoading());
                if (searchDataModel.getErrorMessage() != null) {
                    displayError(searchDataModel.getErrorMessage());
                } else if (searchDataModel.getBusinesses() != null) {
                    displayBusinesses(searchDataModel.getBusinesses());
                }
            }
        });
    }

    private void handleLoadingState(boolean isLoading) {
        findViewById(R.id.search_layout_buttom).setEnabled(!isLoading);
        int visibility = isLoading ? View.VISIBLE : View.GONE;
        findViewById(R.id.search_layout_progressBar).setVisibility(visibility);
    }

    private void displayBusinesses(List<Business> businesses) {
        listAdapter.submitList(businesses);
    }

    private void displayError(String errorMessage) {
        String title = "Error";
        Dialog dialog = new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(errorMessage)
                .setPositiveButton("ok", null)
                .create();
        dialog.show();
    }

    private void clearSearchResult() {
        listAdapter.submitList(null);
    }
}
