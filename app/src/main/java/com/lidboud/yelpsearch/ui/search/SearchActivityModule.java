package com.lidboud.yelpsearch.ui.search;

import android.arch.lifecycle.MutableLiveData;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;

import com.lidboud.yelpsearch.data.datasource.BusinessDataSource;
import com.lidboud.yelpsearch.data.datasource.YelpNetworkBusinessDataSource;
import com.lidboud.yelpsearch.data.model.domain.Business;
import com.lidboud.yelpsearch.repository.BusinessRepository;
import com.lidboud.yelpsearch.repository.YelpBusinessRepository;
import com.lidboud.yelpsearch.usecase.SearchBusinessByTerm;
import com.lidboud.yelpsearch.usecase.YelpSearchBusinessByTerm;

import dagger.Module;
import dagger.Provides;

@Module
public class SearchActivityModule {

    @Provides
    MutableLiveData<SearchDataModel> provideBusinessesData() {
        return new MutableLiveData<>();
    }

    @Provides
    SearchBusinessByTerm provideSearchBusinessByTerm(YelpSearchBusinessByTerm yelpSearchBusinessByTerm) {
        return yelpSearchBusinessByTerm;
    }

    @Provides
    BusinessRepository provideBusinessRepository(YelpBusinessRepository yelpBusinessRepository) {
        return yelpBusinessRepository;
    }

    @Provides
    BusinessDataSource provideBusinessDataSource(YelpNetworkBusinessDataSource yelpNetworkBusinessDataSource) {
        return yelpNetworkBusinessDataSource;
    }

    @Provides
    ListAdapter<Business, BusinessAdapter.ViewHolder> provideBusinessListAdapter(BusinessAdapter businessAdapter) {
        return businessAdapter;
    }

    @Provides
    DiffUtil.ItemCallback<Business> provideBusinessItemCallback() {
        return new BusinessDiffCallback();
    }

}
